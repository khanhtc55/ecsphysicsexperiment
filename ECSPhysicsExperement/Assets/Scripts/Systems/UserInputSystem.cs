﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class UserInputSystem : IExecuteSystem
{
    readonly IGroup<GameEntity> userControlEntities;

    public UserInputSystem(Contexts contexts)
    {
        userControlEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.HeroFlag, GameMatcher.Move));
    }

    public void Execute()
    {
        Vector2 moveRequest = Vector2.zero;
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) moveRequest.y = 1;
        if (Input.GetKey(KeyCode.RightArrow)) moveRequest.x = 1;
        if (Input.GetKey(KeyCode.LeftArrow)) moveRequest.x = -1;


        foreach(GameEntity e in userControlEntities)
        {
            e.move.data.moveRequest = moveRequest;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class BombTrajectorySystem : IExecuteSystem
{
    IGroup<GameEntity> bombEntities;

    public BombTrajectorySystem(Contexts contexts)
    {
        bombEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Move, GameMatcher.BombFlag));
    }

    public void Execute()
    {
        foreach(GameEntity e in bombEntities)
            if(e.move.data.isOnGround)
            {
                e.move.data.moveRequest += new Vector2(0, Random.Range(1, 2.5f));
            }
    }
}

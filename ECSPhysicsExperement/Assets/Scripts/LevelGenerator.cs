﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Entitas.Unity;

public class LevelGenerator
{
    GameContext gameContext;
    PlaySceneManager playSceneManager;
    float countdown = 1f;

    public void Init(GameContext gameContext, PlaySceneManager playSceneManager)
    {
        this.gameContext = gameContext;
        this.playSceneManager = playSceneManager;

        CreateHeroEntity();
    }

    public void OnUpdate(float dt)
    {
        countdown -= dt;
        if (countdown <= 0)
        {
            countdown = Random.Range(3, 5f);
            CreateBombEntity();
        }
    }

    void CreateHeroEntity()
    {
        GameEntity heroEntity = gameContext.CreateEntity();
        heroEntity.AddMove(new MoveComponentData(5));
        heroEntity.isHeroFlag = true;

        GameObjectBehavior goBehavior = GameObject.Instantiate(playSceneManager.heroPrefab) as GameObjectBehavior;
        goBehavior.transform.position = new Vector3(0, 2, 0);
        goBehavior.gameObject.Link(heroEntity, gameContext);
        goBehavior.Init(heroEntity);

        heroEntity.AddGameObject(goBehavior);
    }

    void CreateBombEntity()
    {
        GameEntity bombEntity = gameContext.CreateEntity();
        bombEntity.AddMove(new MoveComponentData(0));
        bombEntity.isBombFlag = true;

        GameObjectBehavior goBehavior = GameObject.Instantiate(playSceneManager.bombPrefab) as GameObjectBehavior;
        goBehavior.transform.position = new Vector3(Random.Range(-8, 8f), Random.Range(3, 4f), 0);
        goBehavior.gameObject.Link(bombEntity, gameContext);
        goBehavior.Init(bombEntity);

        bombEntity.AddGameObject(goBehavior);
    }
}

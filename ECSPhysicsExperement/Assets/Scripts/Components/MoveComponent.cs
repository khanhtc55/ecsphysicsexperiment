﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

[Game]
public class MoveComponent : IComponent
{
    public MoveComponentData data;
}

public class MoveComponentData
{
    public bool isOnGround;
    public Vector2 moveRequest;
    public float maxHorSpeed;

    public MoveComponentData(float maxHorizontalSpeed)
    {
        this.maxHorSpeed = maxHorizontalSpeed;
    }
}
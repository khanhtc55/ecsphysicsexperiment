﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class GameObjectBehavior : MonoBehaviour
{
    public Rigidbody2D rigid;
    GameEntity entity;
    
    public void Init(GameEntity entity)
    {
        this.entity = entity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //khi hệ thống bên ngoài ECS (ở đây là unity physics) tác động đến entity,
        //save lại dữ liệu của sự tác động đó để xử lý trong logic nằm ở systems.
        if (collision.transform.name == "ground") entity.move.data.isOnGround = true;
    }
}

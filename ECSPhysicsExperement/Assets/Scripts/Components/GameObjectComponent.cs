﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

[Game]
public class GameObjectComponent : IComponent
{
    public GameObjectBehavior data;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class MoveSystem : IExecuteSystem
{
    IGroup<GameEntity> moveableEntities;

    public MoveSystem(Contexts contexts)
    {
        moveableEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Move, GameMatcher.GameObject));
    }

    public void Execute()
    {
        foreach (GameEntity e in moveableEntities)
        {
            if (e.move.data.moveRequest.y != 0 && e.move.data.isOnGround) Jump(e);
            MoveHorizontally(e);
            e.move.data.moveRequest = Vector2.zero;
        }
    }

    

    void Jump(GameEntity e)
    {
        //có thể triển khai custom physics ở đây, trong trường hợp này, ta xử dụng bộ physics
        // của unity, nên chỉ gọi api tương ứng để unity xử lý

        float jumpForce = e.move.data.moveRequest.y * 200;
        e.gameObject.data.rigid.AddForce(new Vector2(0, jumpForce));
        e.move.data.isOnGround = false;
    }

    void MoveHorizontally(GameEntity e)
    {
        //có thể triển khai custom physics ở đây, trong trường hợp này, ta xử dụng bộ physics
        // của unity, nên chỉ gọi api tương ứng để unity xử lý

        e.gameObject.data.rigid.velocity = new Vector2(
            e.move.data.maxHorSpeed * e.move.data.moveRequest.x,
            e.gameObject.data.rigid.velocity.y);
    }
}

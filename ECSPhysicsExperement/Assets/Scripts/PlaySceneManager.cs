﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class PlaySceneManager : MonoBehaviour
{
    public GameObjectBehavior heroPrefab, bombPrefab;

    Systems playSceneSystems;
    LevelGenerator levelGenerator;

    void Start()
    {
        var contexts = Contexts.sharedInstance;

        playSceneSystems = new Feature("PlaySceneSystems")
            .Add(new UserInputSystem(contexts))
            .Add(new BombTrajectorySystem(contexts))
            .Add(new MoveSystem(contexts))
            ;

        playSceneSystems.Initialize();


        levelGenerator = new LevelGenerator();
        levelGenerator.Init(contexts.game, this);
    }

    void Update()
    {
        playSceneSystems.Execute();
        playSceneSystems.Cleanup();

        levelGenerator.OnUpdate(Time.deltaTime);
    }
}
